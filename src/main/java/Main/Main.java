package Main;

import DataAccess.RestaurantSerialization;
import Presentation.InitGUI;

public class Main {

    public static void main(String[] args) {

        //Serialization Demo
        RestaurantSerialization sr = new RestaurantSerialization();
        sr.deserialize();

        new InitGUI();
    }
}
