package BusinessLogic;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order {
    public int orderID;
    public Date date;
    public int table;

    public Order(int orderID, Date date, int table) {
        this.orderID = orderID;
        this.date = date;
        this.table = table;
    }

    public Order() {}

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getOrderID() == order.getOrderID() &&
                getTable() == order.getTable() &&
                getDate().equals(order.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrderID(), getDate(), getTable());
    }
}
