package BusinessLogic;

import DataAccess.FileWriter;

import java.util.*;

public class Restaurant extends java.util.Observable implements IRestaurantProcessing {

    private Map<Order, Collection<MenuItem>> orders;
    private ArrayList<MenuItem> menuItems;

    public Restaurant() {
        orders = new HashMap<>();
        menuItems = new ArrayList<>();
    }

    public Map<Order, Collection<MenuItem>> getOrders() {
        return orders;
    }

    public void setOrders(Map<Order, Collection<MenuItem>> orders) {
        this.orders = orders;
    }

    public void addOrder(Order order, Collection<MenuItem> menuItems) {
        orders.put(order, menuItems);
    }

    public void removeOrder(Order order, Collection<MenuItem> menuItems) {
        orders.remove(order, menuItems);
    }

    public void notify(Order order, Collection<MenuItem> menuItems) {
        notifyObservers();
    }

    public void setMenuItems(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void addMenuItem(MenuItem menuItem){
        menuItems.add(menuItem);
    }

    @Override
    public void deleteMenuItem(String name) {
        for (MenuItem m : menuItems) {
            if(m.getName().equals(name)){
                menuItems.remove(m);
            }
        }
    }

    @Override
    public void editMenuItem() {

    }

    @Override
    public double computePrice(Order order) {
        double sum = 0;
        for(MenuItem m : orders.get(order)) {
            sum += m.computePrice();
        }
        return sum;
    }

    @Override
    public void generateBill(String orderID, String date, String tableNr, String orderedStuff, String price) {
        FileWriter fileWriter = new FileWriter();
        fileWriter.generateBill(orderID, date, tableNr, orderedStuff, price);

    }


    // Map<Order, Collection<MenuItem>>  → add&notify, remove
    //lista<menuItem> → add, remove, edit
}
