package BusinessLogic;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    ArrayList<MenuItem> products;
    String name;
    boolean vegan;
    double price;

    public CompositeProduct(String name, boolean vegan, double price, ArrayList<MenuItem> products, String name1, boolean vegan1, double price1) {
        super(name, vegan, price);
        this.products = products;
        this.name = name1;
        this.vegan = vegan1;
        this.price = price1;
    }

    public CompositeProduct(ArrayList<MenuItem> products, String name, boolean vegan, double price) {
        this.products = products;
        this.name = name;
        this.vegan = vegan;
        this.price = price;
    }

    public ArrayList<MenuItem> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<MenuItem> products) {
        this.products = products;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isVegan() {
        return vegan;
    }

    @Override
    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double computePrice() {
        price = 0;
        for(MenuItem mi : products) {
            price += mi.computePrice();
        }
        return price;
    }
}
