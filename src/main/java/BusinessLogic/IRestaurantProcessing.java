package BusinessLogic;

import java.util.ArrayList;
import java.util.Collection;

public interface IRestaurantProcessing {

    //operatiile accesibile pentru admin
    void addMenuItem(MenuItem menuItem);
    void deleteMenuItem(String name);
    void editMenuItem();

    //operatiile accesibile pentru chelner
    //void createOrder(ArrayList<String> data);
    void addOrder(Order order, Collection<MenuItem> menuItems);
    double computePrice(Order order);
    void generateBill(String orderID, String date, String tableNr, String orderedStuff, String price);
}
