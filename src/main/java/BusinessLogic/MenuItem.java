package BusinessLogic;

import java.io.Serializable;

public class MenuItem implements Serializable {

    String name;
    boolean vegan;
    double price;

    public MenuItem(String name, boolean vegan, double price){
        this.name = name;
        this.vegan = vegan;
        this.price = price;
    }

    public MenuItem() {

    }

    public double computePrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    @Override
    public String toString() {
        String menuitem = "";
        String vegan = "vegan";
        if (!isVegan()) {
            vegan = "not vegan";
        }
        menuitem =  String.format("\n%50s, %9s, ............ $%.2f", name, vegan, price);
        return menuitem;
    }

}
