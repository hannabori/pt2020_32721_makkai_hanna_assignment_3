package BusinessLogic;

import java.util.Objects;

public class BaseProduct extends MenuItem {
    String name;
    boolean vegan;
    double price;

    public BaseProduct(String name, boolean vegan, double price) {
        this.name = name;
        this.vegan = vegan;
        this.price = price;
    }

    public BaseProduct(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    public double computePrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseProduct)) return false;
        BaseProduct that = (BaseProduct) o;
        return isVegan() == that.isVegan() &&
                Double.compare(that.computePrice(), computePrice()) == 0 &&
                getName().equals(that.getName());
    }

   @Override
    public int hashCode() {
        int hash = Objects.hash(getName(), isVegan(), computePrice());
        return hash;
    }

    @Override
    public String toString() {
        String name = "";
        /*return  "" + name + '\'' +
                ", vegan=" + vegan +
                ", price=" + price +
                '}';*/
        return name;
    }
}
