package Presentation;

import BusinessLogic.Restaurant;
import DataAccess.RestaurantSerialization;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TableMouseListener  extends MouseAdapter {
    private final JTable table;

    public TableMouseListener(JTable table) {
        this.table = table;
    }

    public void mouseClicked(MouseEvent e) {
        //Restaurant restaurant;

        RestaurantSerialization ser = new RestaurantSerialization();
        int column = table.getColumnModel().getColumnIndexAtX(e.getX());
        if (column == 3 && table.getSelectedRowCount() == 1) {
            JTable table = (JTable) e.getSource();
            int modelRow = ((JTable) e.getSource()).getSelectedRow();
            ((DefaultTableModel) table.getModel()).removeRow(modelRow);
            //ser.serialize(restaurant.getMenuItems());

            JOptionPane.showMessageDialog(table, "Item deleted!");

        } else if (column == 3 && table.getSelectedRowCount() > 1) {
            JOptionPane.showMessageDialog(table, "Can not delete multiple items! Please select a single row!");
        }
    }
}
