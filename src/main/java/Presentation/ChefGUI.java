package Presentation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import static java.awt.Color.*;

public class ChefGUI extends JFrame implements java.util.Observer {

    JPanel p; //panelul care contine elementele urmatoare de graphic user interface (GUI), chenarul principal
    JLabel l1;
    //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    //String dateString = format.format( new Date());
    //Date date       = format.parse( "2009-12-31" );
    //Object orders[] = {new Order(3, date, 8)};
    JList list;
    public final JButton back = new JButton("<<BACK");

    public ChefGUI(){
        init();
    }

    public void init() {
        setSize(640, 370);
        setTitle("Orders");

        p = new JPanel();//panelul continand toate elementele din GUI
        p.setBackground(black);//culoarea fundalului
        p.setForeground(pink); // culoarea scrisului

        //eticheta cu rol de ghidare
        l1 = new JLabel("Orders received:");
        l1.setBounds(20, 10, 200, 30);
        l1.setFont(l1.getFont().deriveFont(18f));
        /*
         * dimensiunile chenarului:
         * (
         * x = distanta de la marginea stanga,
         * y = distanta de a marginea de sus,
         * width = latimea,
         * height = inaltimea.
         * )
         */
        l1.setBackground(black);
        l1.setForeground(pink);
        //l1.setOpaque(true);
        p.add(l1); //adaugarea etichetei la panel

        //list = new JList(orders);

        back.setBounds(20, 150, 90, 30);
        back.setFont(back.getFont().deriveFont(14f));
        back.setBackground(magenta);
        back.setForeground(black);
        p.add(back);
        back.addActionListener(e -> {
            new InitGUI();
            ChefGUI.this.dispose();
        });

        p.setLayout(null); //salveaza setarile grafice punand layoutul initial la null
        p.setOpaque(true);
        this.add(p); //adaugarea panelului de GUI la containerul principal
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE); //fereastra de GUI se inchide la apasarea butonului X din colt
        this.setVisible(true); //seteaza vizibilitatea GUI-ului la 100%

    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("FirstNewsReader got The news:"+(String)arg);
    }
}
