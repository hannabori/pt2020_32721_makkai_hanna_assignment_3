package Presentation;

import BusinessLogic.MenuItem;
import BusinessLogic.Restaurant;
import DataAccess.RestaurantSerialization;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import static java.awt.Color.*;
import static java.awt.Color.magenta;

public class AdminGUI extends JFrame implements ActionListener {

    private Restaurant restaurant = new Restaurant();

    private JPanel p; //panelul care contine elementele urmatoare de graphic user interface (GUI), chenarul principal
    final JButton viewAll = new JButton("View All");
    final JButton newMenuItem = new JButton("New Menu Item");
    final JButton back = new JButton("<<BACK");
    final JLabel name = new JLabel("Name:");
    final JLabel vegan = new JLabel("Vegan?");
    final JLabel price = new JLabel("Price:");
    private final JButton addBtn = new JButton("ADD");
    private final JButton update = new JButton("UPDATE");
    private JTextField nameField = new JTextField();
    private JComboBox<ComboItem> veganField = new JComboBox<>();
    private JTextField priceField = new JTextField();

    private Color myPink = new Color(214,104,155);

    private String[] columns = {"Name", "Vegan", "Price", "Delete"};
    private DefaultTableModel tableModel = new DefaultTableModel(columns, 0) {
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };
    private JTable allMenuItems = new JTable(tableModel);

    AdminGUI(){
        init();
    }

    void init() {
        setSize(640, 370);
        setTitle("Menu Items");

        p = new JPanel();//panelul continand toate elementele din GUI
        p.setBackground(black);//culoarea fundalului
        p.setForeground(pink); // culoarea scrisului

        /*
         * dimensiunile chenarului la elemente adaugate la panel:
         * (
         * x = distanta de la marginea stanga,
         * y = distanta de a marginea de sus,
         * width = latimea,
         * height = inaltimea.
         * )
         */

        viewAll.setBounds(20, 10, 140, 30);
        viewAll.setFont(viewAll.getFont().deriveFont(14f));
        viewAll.setBackground(darkGray);
        viewAll.setForeground(pink);
        p.add(viewAll);
        viewAll.addActionListener( this);

        /*newMenuItem.setBounds(170, 10, 140, 30);
        newMenuItem.setFont(newMenuItem.getFont().deriveFont(14f));
        newMenuItem.setBackground(darkGray);
        newMenuItem.setForeground(pink);
        p.add(newMenuItem);
        newMenuItem.addActionListener(this);*/

        name.setBounds(20, 200, 230, 20);
        name.setForeground(pink);
        name.setBackground(darkGray);
        p.add(name);

        vegan.setBounds(285, 200, 50, 20);
        vegan.setForeground(pink);
        vegan.setBackground(darkGray);
        p.add(vegan);

        price.setBounds(350, 200, 50, 20);
        price.setForeground(pink);
        price.setBackground(darkGray);
        p.add(price);

        nameField.setBounds(20, 230, 260, 30);
        nameField.setBackground(pink);
        p.add(nameField);

        veganField.setBounds(285, 230, 60, 30);
        veganField.setBackground(pink);

        veganField.addItem(new ComboItem("yes", "yes"));
        veganField.addItem(new ComboItem("no", "no"));
        p.add(veganField);

        priceField.setBounds(350, 230, 70, 30);
        priceField.setBackground(pink);
        p.add(priceField);

        addBtn.setBounds(435, 230, 85, 30);
        addBtn.setBackground(myPink);
        addBtn.setForeground(black);
        p.add(addBtn);
        addBtn.addActionListener(this);

        update.setBounds(525, 230, 85, 30);
        update.setBackground(myPink);
        update.setForeground(black);
        p.add(update);
        update.addActionListener(this);

        back.setBounds(20, 280, 90, 30);
        back.setFont(back.getFont().deriveFont(14f));
        back.setBackground(magenta);
        back.setForeground(black);
        p.add(back);
        back.addActionListener(e -> {
            new InitGUI();
            AdminGUI.this.dispose();
        });

        allMenuItems.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                DefaultTableModel tableModel = (DefaultTableModel)allMenuItems.getModel();

                String name = tableModel.getValueAt(allMenuItems.getSelectedRow(), 0).toString();
                String vegan = tableModel.getValueAt(allMenuItems.getSelectedRow(), 1).toString();
                String price = tableModel.getValueAt(allMenuItems.getSelectedRow(), 2).toString();

                nameField.setText(name);
                veganField.setToolTipText(vegan);
                priceField.setText(price);
                //super.mouseClicked(e);
            }
        });

        TableMouseListener t = new TableMouseListener(allMenuItems);
        allMenuItems.addMouseListener(t);

        p.setLayout(null); //salveaza setarile grafice punand layoutul initial la null
        p.setOpaque(true);
        this.add(p); //adaugarea panelului de GUI la containerul principal
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE); //fereastra de GUI se inchide la apasarea butonului X din colt
        this.setVisible(true); //seteaza vizibilitatea GUI-ului la 100%

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RestaurantSerialization ser = new RestaurantSerialization();

        if (e.getSource() == this.viewAll) {
            ArrayList<MenuItem> menuItems = ser.deserialize();
            for (int i = 0; i < menuItems.size(); i++){
                String name = menuItems.get(i).getName();
                boolean vegan = menuItems.get(i).isVegan();
                String veganStr = "yes";
                if (!vegan) {
                    veganStr = "no";
                }
                String price = "" + menuItems.get(i).computePrice();

                Object[] data = {name,  veganStr, price, "Delete"};
                tableModel.addRow(data);
            }

            //Demo
            /*tableModel.addRow(new Object[]{"Salmon with fruit and sweet potatoes", "no", "$27.99", "Delete"});
            tableModel.addRow(new Object[]{"Caesar salad with roasted chicken stripes", "no", "$32.45", "Delete"});
            tableModel.addRow(new Object[]{"Cashew cake with blueberries and raspberries", "yes", "$18.74", "Delete"});*/

            allMenuItems.getColumn("Delete").setCellRenderer(new ButtonRenderer());
            allMenuItems.setForeground(pink);
            allMenuItems.setBackground(darkGray);

            JTableHeader header = allMenuItems.getTableHeader();
            header.setBackground(darkGray);
            header.setForeground(magenta);
            header.setFont(header.getFont().deriveFont(15f));

            JScrollPane scrollPane = new JScrollPane(allMenuItems);
            allMenuItems.setPreferredScrollableViewportSize(allMenuItems.getPreferredSize());
            allMenuItems.getColumnModel().getColumn(0).setPreferredWidth(240);
            allMenuItems.getColumnModel().getColumn(1).setPreferredWidth(50);
            allMenuItems.getColumnModel().getColumn(2).setPreferredWidth(50);
            allMenuItems.getColumnModel().getColumn(3).setPreferredWidth(60);

            scrollPane.setBounds(20, 50, 590, 130);
            scrollPane.getViewport().setBackground(darkGray);
            scrollPane.getViewport().setForeground(pink);
            scrollPane.setOpaque(true);
            p.add(scrollPane);

        } else if (e.getSource() == this.addBtn) {

            try {
                String name = nameField.getText();
                boolean veganValue = false;
                if (veganField.getSelectedItem() == "yes") {
                    veganValue = true;
                }
                double priceValue;
                String price = priceField.getText();
                if (price != null) {
                    try {
                        priceValue = Double.parseDouble(priceField.getText());
                    } catch (NumberFormatException ex) {
                        priceValue = 0;
                        JOptionPane.showMessageDialog(this, "Price can't be empty!");
                    }
                } else {
                    priceValue = 0;
                }

                boolean exists = false;
                String s = "";

                for (int i = 0; i < allMenuItems.getRowCount(); i++) {
                    s = allMenuItems.getValueAt(i, 0).toString().trim();

                    if (name.equals("")) {
                        JOptionPane.showMessageDialog(this, "Name must be specified!");
                    } else {
                        if (name.equals(s)) {
                            exists = true;
                            break;
                        }
                    }
                }

                if (!exists) {
                    if (!name.equals("") && !price.equals("")) {
                        MenuItem menuItem = new MenuItem(name, veganValue, priceValue);
                        restaurant.addMenuItem(menuItem);
                        ser.serialize(restaurant.getMenuItems());

                        tableModel.addRow(new Object[]{name, veganField.getSelectedItem(), price, "Delete"});
                    } else {
                        JOptionPane.showMessageDialog(this, "Name and price must be specified!");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Item already exits!");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,"Something went wrong, please double check the values entered!");
            }

        } else if (e.getSource() == this.update) {
            DefaultTableModel tableModel = (DefaultTableModel)allMenuItems.getModel();
            if (allMenuItems.getSelectedRowCount() == 1) {
                try {
                    String name = nameField.getText();
                    Object vegan = veganField.getSelectedItem();
                    boolean veganValue = false;
                    if (veganField.getSelectedItem() == "yes") {
                        veganValue = true;
                    }
                    String price = priceField.getText();
                    double priceValue = Double.parseDouble(price);

                    tableModel.setValueAt(name, allMenuItems.getSelectedRow(), 0);
                    tableModel.setValueAt(vegan, allMenuItems.getSelectedRow(), 1);
                    tableModel.setValueAt(price, allMenuItems.getSelectedRow(), 2);

                    MenuItem menuItem = new MenuItem(name, veganValue, priceValue);
                    restaurant.addMenuItem(menuItem);
                    ser.serialize(restaurant.getMenuItems());

                    JOptionPane.showMessageDialog(this, "Update successful!");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(this,"Something went wrong, please double check the values entered!");
                }
            } else if (allMenuItems.getRowCount() == 0) {
                JOptionPane.showMessageDialog(this, "Table is empty!");
            } else if (allMenuItems.getSelectedRowCount() > 1){
                JOptionPane.showMessageDialog(this, "Update can not be performed! Please select a single row!");
            } else if (allMenuItems.getSelectedRowCount() == 0) {
                JOptionPane.showMessageDialog(this, "Please select a row to update!");
            }

        }
    }

}

