package Presentation;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.Color.*;

public class InitGUI extends JFrame {
    private JPanel p; //panelul care contine elementele urmatoare de graphic user interface (GUI), chenarul principal
    private JLabel l1;
    private final JButton admin = new JButton("Administrator");
    private final JButton waiter = new JButton("Waiter");
    private final JButton chef = new JButton("Chef");

    private Color myPink = new Color(214,104,155);

    public InitGUI() {
        init();
    }

    void init(){
        setSize(250, 240);
        setTitle("Welcome!");

        p = new JPanel();//panelul continand toate elementele din GUI
        p.setBackground(black);//culoarea fundalului
        p.setForeground(pink); // culoarea scrisului

        //eticheta cu rol de ghidare
        l1 = new JLabel("Who are you?");
        l1.setBounds(20, 10, 200, 35);
        l1.setFont(l1.getFont().deriveFont(18f));
        /*
         * dimensiunile chenarului:
         * (
         * x = distanta de la marginea stanga,
         * y = distanta de a marginea de sus,
         * width = latimea etichetei,
         * height = inaltimea etichetei
         * )
         */
        l1.setBackground(black);
        l1.setForeground(myPink);
        //l1.setOpaque(true);
        p.add(l1); //adaugarea etichetei la panel

        admin.setBounds(20, 50, 200, 35);
        admin.setFont(admin.getFont().deriveFont(18f));
        admin.setBackground(darkGray);
        admin.setForeground(magenta);
        p.add(admin);
        admin.addActionListener(e -> {
            new AdminGUI();
            InitGUI.this.dispose();
        });

        waiter.setBounds(20, 100, 200, 35);
        waiter.setFont(waiter.getFont().deriveFont(18f));
        waiter.setBackground(darkGray);
        waiter.setForeground(magenta);
        p.add(waiter);
        waiter.addActionListener(e -> {
            new WaiterGUI();
            InitGUI.this.dispose();
        });

        chef.setBounds(20, 150, 200, 33);
        chef.setFont(chef.getFont().deriveFont(18f));
        chef.setBackground(darkGray);
        chef.setForeground(magenta);
        p.add(chef);
        chef.addActionListener(e -> {
            new ChefGUI();
            InitGUI.this.dispose();
        });

        p.setLayout(null); //salveaza setarile grafice punand layoutul initial la null
        p.setOpaque(true);
        this.add(p); //adaugarea panelului de GUI la containerul principal
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE); //fereastra de GUI se inchide la apasarea butonului X din colt
        this.setVisible(true); //seteaza vizibilitatea GUI-ului la 100%
    }
}
