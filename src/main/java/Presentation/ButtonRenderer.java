package Presentation;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

class ButtonRenderer extends JButton implements TableCellRenderer {

    ButtonRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (isSelected) {
            setForeground(Color.darkGray);
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(Color.magenta);
            setBackground(Color.darkGray);
        }
        setText((value == null) ? "" : value.toString());
        return this;
    }
}
