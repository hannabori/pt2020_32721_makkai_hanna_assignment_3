package Presentation;

import java.io.Serializable;

public class ComboItem implements Serializable {
    private String key;
    private String value;

    ComboItem(String key, String value)
    {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString()
    {
        return key;
    }

    public String getKey()
    {
        return key;
    }

    String getValue()
    {
        return value;
    }
}
