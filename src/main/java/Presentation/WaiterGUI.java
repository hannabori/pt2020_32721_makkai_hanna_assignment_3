package Presentation;

import BusinessLogic.Order;
import DataAccess.FileWriter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static java.awt.Color.*;

public class WaiterGUI extends JFrame implements ActionListener {
    JPanel p; //panelul care contine elementele urmatoare de graphic user interface (GUI), chenarul principal
    private final JButton back = new JButton("<<BACK");
    private final JButton viewAll = new JButton("View All");
    private final JLabel orderID = new JLabel("Order Nr:");
    private final JLabel date = new JLabel("Date:");
    private final JLabel table = new JLabel("Table:");
    private final JLabel orderedItems = new JLabel("Ordered items:");
    private final JButton addBtn = new JButton("ADD");
    private final JButton billBtn = new JButton("BILL");
    private JTextField orderIDField = new JTextField();
    private DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private JFormattedTextField dateField = new JFormattedTextField(format);
    private JTextField tableField = new JTextField();
    private JTextField orderedItemsField = new JTextField();

    private Color myPink = new Color(214, 104, 155);

    private String[] columns = {"Order Nr.", "Date", "Table", "Ordered Items"};
    private DefaultTableModel tableModel = new DefaultTableModel(columns, 0) {
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    private JTable allOrders = new JTable(tableModel);

    WaiterGUI() {
        init();
    }

    private void init() {
        setSize(640, 420);
        setTitle("Orders");

        p = new JPanel();//panelul continand toate elementele din GUI
        p.setBackground(black);//culoarea fundalului
        p.setForeground(pink); // culoarea scrisului

        /*
         * dimensiunile chenarului la elemente adaugate la panel:
         * (
         * x = distanta de la marginea stanga,
         * y = distanta de a marginea de sus,
         * width = latimea,
         * height = inaltimea.
         * )
         */

        viewAll.setBounds(20, 10, 140, 30);
        viewAll.setFont(viewAll.getFont().deriveFont(13f));
        viewAll.setBackground(darkGray);
        viewAll.setForeground(pink);
        p.add(viewAll);
        viewAll.addActionListener(this);

        orderID.setBounds(20, 200, 230, 20);
        orderID.setForeground(pink);
        orderID.setBackground(darkGray);
        p.add(orderID);

        date.setBounds(285, 200, 50, 20);
        date.setForeground(pink);
        date.setBackground(darkGray);
        p.add(date);

        table.setBounds(350, 200, 50, 20);
        table.setForeground(pink);
        table.setBackground(darkGray);
        p.add(table);

        orderIDField.setBounds(20, 230, 260, 30);
        orderIDField.setBackground(pink);
        p.add(orderIDField);

        orderedItems.setBounds(20, 270, 100, 30);
        orderedItems.setForeground(pink);
        orderedItems.setBackground(darkGray);
        p.add(orderedItems);

        orderedItemsField.setBounds(130,270,290,30);
        orderedItemsField.setBackground(pink);
        p.add(orderedItemsField);

        dateField.setBounds(285, 230, 60, 30);
        dateField.setBackground(pink);
        dateField.setText("dd/MM/yyyy");
        p.add(dateField);

        tableField.setBounds(350, 230, 70, 30);
        tableField.setBackground(pink);
        p.add(tableField);


        addBtn.setBounds(435, 270, 85, 30);
        addBtn.setBackground(myPink);
        addBtn.setForeground(black);
        p.add(addBtn);
        addBtn.addActionListener(this);

        billBtn.setBounds(525, 270, 85, 30);
        billBtn.setBackground(myPink);
        billBtn.setForeground(black);
        p.add(billBtn);
        billBtn.addActionListener(this);

        allOrders.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                DefaultTableModel tableModel = (DefaultTableModel)allOrders.getModel();

                String orderID = tableModel.getValueAt(allOrders.getSelectedRow(), 0).toString();
                String date = tableModel.getValueAt(allOrders.getSelectedRow(), 1).toString();
                String table = tableModel.getValueAt(allOrders.getSelectedRow(), 2).toString();
                String orderedItems = tableModel.getValueAt(allOrders.getSelectedRow(), 3).toString();

                orderIDField.setText(orderID);
                dateField.setText(date);
                tableField.setText(table);
                orderedItemsField.setText(orderedItems);
            }
        });


        back.setBounds(20, 330, 90, 30);
        back.setFont(back.getFont().deriveFont(14f));
        back.setBackground(magenta);
        back.setForeground(black);
        p.add(back);
        back.addActionListener(e -> {
            new InitGUI();
            WaiterGUI.this.dispose();
        });

        p.setLayout(null); //salveaza setarile grafice punand layoutul initial la null
        p.setOpaque(true);
        this.add(p); //adaugarea panelului de GUI la containerul principal
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE); //fereastra de GUI se inchide la apasarea butonului X din colt
        this.setVisible(true); //seteaza vizibilitatea GUI-ului la 100%
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.viewAll) {
            //Demo
            /*tableModel.addRow(new Object[]{"125", "12/12/2019", "7"});
            tableModel.addRow(new Object[]{"126", "12/12/2019", "3"});
            tableModel.addRow(new Object[]{"125", "12/12/2019", "7"});
            tableModel.addRow(new Object[]{"125", "12/12/2019", "7"});
            tableModel.addRow(new Object[]{"126", "12/12/2019", "3"});
            tableModel.addRow(new Object[]{"125", "12/12/2019", "7"});
            tableModel.addRow(new Object[]{"125", "12/12/2019", "7"});
            tableModel.addRow(new Object[]{"126", "12/12/2019", "3"});
            tableModel.addRow(new Object[]{"125", "12/12/2019", "7"});*/

            allOrders.setForeground(pink);
            allOrders.setBackground(darkGray);

            JTableHeader header = allOrders.getTableHeader();
            header.setBackground(darkGray);
            header.setForeground(magenta);
            header.setFont(header.getFont().deriveFont(15f));

            JScrollPane scrollPane = new JScrollPane(allOrders);
            allOrders.setPreferredScrollableViewportSize(allOrders.getPreferredSize());
            allOrders.getColumnModel().getColumn(0).setPreferredWidth(50);
            allOrders.getColumnModel().getColumn(1).setPreferredWidth(50);
            allOrders.getColumnModel().getColumn(2).setPreferredWidth(50);
            allOrders.getColumnModel().getColumn(3).setPreferredWidth(60);

            scrollPane.setBounds(20, 50, 590, 130);

            scrollPane.getViewport().setBackground(darkGray);
            scrollPane.getViewport().setForeground(pink);
            scrollPane.setOpaque(true);
            p.add(scrollPane);

        } else if (e.getSource() == this.addBtn) {
                String orderID = orderIDField.getText();
                String date = dateField.getText();
                String table = tableField.getText();
                String items = orderedItemsField.getText();
                String str[] = items.split(", ");


            try {
                int orderNr = Integer.parseInt(orderID);
                Date date1 = format.parse(date);
                int tableNr = Integer.parseInt(table);

                Order order = new Order(orderNr, date1, tableNr);

                if (!orderID.equals("") && !table.equals("")) {
                    tableModel.addRow(new Object[]{orderID, date, tableNr, items});
                }
            } catch (ParseException | NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Order and table numbers must be integers!");
            }

        } else if (e.getSource() == this.billBtn) {

            if (allOrders.getSelectedRowCount() == 1) {
                String orderID = orderIDField.getText();
                String date = dateField.getText();
                String tableNr = tableField.getText();
                String orderedStuff = orderedItemsField.getText();
                String price = "";
                //price = menuItem.computePrice();

                FileWriter fw = new FileWriter();
                fw.generateBill(orderID, date, tableNr, orderedStuff, price);

                int modelRow = allOrders.getSelectedRow();
                ((DefaultTableModel) allOrders.getModel()).removeRow(modelRow);
            } else if (allOrders.getSelectedRowCount() == 0) {
                JOptionPane.showMessageDialog(this, "Please select an order to bill!");
            }
        }

    }
}
