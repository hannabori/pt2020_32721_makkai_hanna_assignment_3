package DataAccess;

import BusinessLogic.MenuItem;
import javafx.animation.ScaleTransition;

import javax.swing.table.DefaultTableModel;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RestaurantSerialization implements Serializable {

    public void serialize(ArrayList<MenuItem> obj) {

        //demo
        /*obj.add(new MenuItem("Salmon with fruit and sweet potatoes", false, 47.99));
        obj.add(new MenuItem("Octopus cooked in soy sauce and mayo cream", false, 35.99));
        obj.add(new MenuItem("apple", true, 1.45));
        obj.add(new MenuItem("ginger beer", true, 4.89));*/

        try {
            FileOutputStream fileOut = new FileOutputStream("menuitems.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            out.close();
            fileOut.close();
            System.out.println("\nSerialization successful! Check your output file!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MenuItem> deserialize() {
        ArrayList<MenuItem> menuItems = new ArrayList<>();

        try {
            FileInputStream fileIn = new FileInputStream("menuitems.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            menuItems = (ArrayList) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException | ClassNotFoundException  e) {
            e.printStackTrace();
        }

        System.out.println("\nDeserialized Menu Items:\n" + menuItems.toString());
        return menuItems;
    }

}
