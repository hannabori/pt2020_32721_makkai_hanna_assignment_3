package DataAccess;

import BusinessLogic.IRestaurantProcessing;

import java.io.BufferedWriter;
import java.io.IOException;

public class FileWriter {

    public void generateBill(String orderID, String date, String tableNr, String orderedStuff, String price) {
        try {
            BufferedWriter writer = new BufferedWriter(new java.io.FileWriter("bill" + orderID  + ".txt"));
            writer.write(String.format("\nWelcome!" +
                            "\n-----------------------------------------------\n\n" +
                            "Order number: %s\nTable number: %s" +
                            "\n-----------------------------------------------\n\n" +
                            "Your order:\n%s" +
                            "\nTotal:$%s" +
                            "\n\n-----------------------------------------------\n\n" +
                            "Have a nice day!\n\n" +
                            "Date:\n%s",
                    orderID, tableNr, orderedStuff, price , date));
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }
}
